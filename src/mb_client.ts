import * as net from "net";
import {Observable} from "rxjs/Observable";
import {fromEvent} from "rxjs/observable/fromEvent";
import Socket = NodeJS.Socket;
import {connect} from "net";
import {isUndefined} from "util";
const settings = require('./../settings.json');

export class MbClient {

    private socket: net.Socket;
    private can_emit: boolean;

    constructor(lsettings?:any) {
        this.can_emit = false;
        if(lsettings === null || isUndefined(lsettings)) {
            lsettings = settings;
        }
        this.socket = connect(lsettings, ()=> {
            this.can_emit = true;
        });

        this.socket.on('error',(err)=> {
            console.log(err);
        });
    }

    data():Observable<any> {
        return fromEvent(this.socket, 'data');
    }

    close():Observable<any> {
        return fromEvent(this.socket, 'close');
    }

    end():Observable<any> {
        return fromEvent(this.socket, 'end');
    }

    cmd(cmd: string) {
        this.socket.write(cmd + '\n');
    }
}