import {createServer, Server} from "http";
import * as sio from "socket.io";
import * as net from "net";
import {Observable} from "rxjs/Observable";
import {fromEvent} from "rxjs/observable/fromEvent";
import "rxjs/add/operator/map";
import {MbClient} from "./mb_client";

let socketio = require('socket.io');
let express = require('express');


const settings = require('./../settings.json');
const bodyParser = require('body-parser');

export class AppServer {
    public app: any;
    public server: any;
    public io: SocketIO.Server;

    constructor() {
        this.app = express();
        this.server = createServer(this.app);
        this.io = socketio(this.server, {
            reconnection: true,
            reconnectionDelay: 1000,
            reconnectionDelayMax : 5000,
            reconnectionAttempts: Infinity,
            origins: ['*:*','http://localhost:4200']
        });

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
    }

    run() {
        this.server.listen(settings.serverport, ()=> {
            console.log("The server run: http://localhost:"+settings.serverport );
        });

        this.user_connect()
            .map(this.create_user_session)
            .subscribe();
    }

    create_user_session(socket: any) {
        console.log("Create user session");
        //console.log(socket);
        let mb_client = new MbClient();
        socket.emit('server-event', 'Connection established');
        mb_client.data()
            .subscribe((data)=> {

                let result;
                try {
                    result = JSON.parse(data.toString('utf8'));
                    if (result['event'] != -2) {
                        console.log(result);
                    }
                    socket.emit('server-event', result);
                } catch (e) {
                    console.log(e);
                    console.log(data.toString('utf8'));
                }

            });

        socket.on('cmd', (cmd) => {
            console.log('socket cmd', cmd);
           mb_client.cmd(cmd);
        });

        socket.on('disconnect', () => {
            console.log('Disconnect !');
            socket.disconnect();
            mb_client.close();
            mb_client.end();
            clearInterval(idInterval);
        });

        let idInterval = setInterval(()=> {
            mb_client.cmd('progress');
        }, 1000);
    }

    user_connect():Observable<any> {
        console.log('USer COnnect');
        return new Observable<any>(subscriber => {
            console.log("Connection");
            this.io.on('connection', (socket) => {
                subscriber.next(socket);
                //subscriber.complete();
            })
        });
    }

}

let app = new AppServer();
app.run();