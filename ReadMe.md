# Musicbot Node

## About

The following project is an HTTP interface for the MusicBot project.

The server provide an interface between the user HTTP interactions and the TCP sockets with the
MusicBot Server.

## Roadmap

- Add documentation
- Log
- Display an error msg when the node server cannot be connected to the musicbot server

## Prerequesites

- Node > 7

## Install

```sh
    $ npm install

```

## Run

```sh
    $ npm start
```